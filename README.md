# Helpdesk PWA

Our goal is the development of a simple yet complete call answering system that allows for some basic functionality:
- Receive, interact and close calls;
- Treatment for workflows;
- Receipt of images, movies, sound files and possibility of remote access;
- Metrics for organizations, users and servers;